# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#importing libraries
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

#importing datasets
dataset_train = pd.read_csv('train.csv')
dataset_test = pd.read_csv('test.csv')
x_train = dataset_train.iloc[:,[2,4,5,6,7,9]].values
x_test = dataset_test.iloc[:,[1,3,4,5,6,8]].values
y_train = dataset_train.iloc[:,1].values

#take care of missing values
from sklearn.preprocessing import Imputer
imputer = Imputer( strategy = 'mean', axis = 0)
imputer.fit(x_train[:, 2:])
x_train[:,2:]=imputer.transform(x_train[:, 2:])
imputer.fit(x_test[:, 2:])
x_test[:,2:]=imputer.transform(x_test[:,2:])

#categorical encoding
from sklearn.preprocessing import LabelEncoder,OneHotEncoder
le_train = LabelEncoder()
x_train[:,1]=le_train.fit_transform(x_train[:,1])
le_test = LabelEncoder()
x_test[:,1]=le_test.fit_transform(x_test[:,1])

#feature scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()

x_train  = sc.fit_transform(x_train)
x_test= sc.transform(x_test)


#Decisiontree model building
from sklearn.tree import DecisionTreeClassifier
classifier = DecisionTreeClassifier(criterion='entropy',random_state=0)
classifier.fit(x_train,y_train)

#predicting the test set results
y_pred= classifier.predict(x_test)




#Ann model building
#import keras
#from keras.models import Sequential
#from keras.layers import Dense
#classifier  = Sequential()
#classifier.add(Dense(activation='relu',input_dim=6,units=10,kernel_initializer='uniform'))
#classifier.add(Dense(activation='relu',units=10,kernel_initializer='uniform'))
#classifier.add(Dense(activation='sigmoid',units=1,kernel_initializer='uniform'))
#classifier.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
#classifier.fit(x_train,y_train,batch_size=82,epochs=128)
#y_p=classifier.predict(x_test)
#y_p = (y_p > 0.5)


y_test =  pd.read_csv('gender_submission.csv')
y_test=y_test.iloc[:,0].values
submissions=pd.DataFrame({'PassengerId':y_test,'Survived':y_pred[0]})
submissions.to_csv('submissions.csv',index=False)

 
